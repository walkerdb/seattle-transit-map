import {lines} from "./LightRailInitiatives";

const pinks = [
  "#49006a",
  "#7a0177",
  "#ae017e",
  "#dd3497",
  "#f768a1",
  "#fa9fb5",
  "#fcc5c0",
]

const blues = [
  "#023858",
  "#045a8d",
  "#0570b0",
  "#3690c0",
  "#74a9cf",
  "#a6bddb",
  "#d0d1e6",
];

const greens = [
  "#00441b",
  "#006d2c",
  "#238b45",
  "#41ab5d",
  "#74c476",
  "#a1d99b",
  "#c7e9c0",
];

const divergentGoodBad = [
  "#b2182b",
  "#d6604d",
  "#f4a582",
  "#fddbc7",
  "#f7f7f7",
  "#d1e5f0",
  "#92c5de",
  "#4393c3",
  "#2166ac",
];

const transitColors = {
  "lightRail": pinks[0]
};

const zoneColors = {
  "Highrise": divergentGoodBad[8],
  "Midrise": divergentGoodBad[8],
  "Lowrise": divergentGoodBad[7],
  "Residential/Commercial": divergentGoodBad[6],
  "Neighborhood/Commercial": divergentGoodBad[6],
  "Downtown": divergentGoodBad[5],
  "Major Institutions": divergentGoodBad[5],
  "Master Planned Community": divergentGoodBad[4],
  "Auto Commerical": divergentGoodBad[3],
  "Manufacturing/Industrial": divergentGoodBad[2],
  "Single Family": divergentGoodBad[0],
}

export {
  transitColors,
  zoneColors,
  divergentGoodBad,
  pinks,
  greens,
  blues
}