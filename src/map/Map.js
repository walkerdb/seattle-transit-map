import React, {Component} from 'react'
import {Map, TileLayer} from 'react-leaflet'
import {lines, routes} from "./LightRailInitiatives";
import LightRailLegend from "./layers/LightRailLegend"
import StationMarker from "./layers/StationMarker"
import TransitRoute from "./layers/TransitRoute"
import ExistingLightRailRoutes from "./layers/ExistingLightRailRoutes"
import HousingZones from "./layers/SeattleHousingZones"

const generateStations = (segments) => {
  return segments.map(segment => segment.initiative.stations.map(station => <StationMarker station={station} segment={segment}/>))
};

const generateRoutes = (segments) => {
  return segments.map(segment => <TransitRoute segment={segment}/>);
};

export default class LinkMap extends Component {
  constructor() {
    super();
    this.state = {
      zoom: 12,
      routes: routes
    }
  }

  render() {
    const downtownSeattle = [47.6062, -122.3321];
    return (
      <Map center={downtownSeattle} zoom={this.state.zoom}>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png'
        />
        <HousingZones/>
        {generateRoutes(this.state.routes[lines.GREEN])}
        {generateRoutes(this.state.routes[lines.BLUE])}
        {generateRoutes(this.state.routes[lines.RED])}
        <ExistingLightRailRoutes/>
        {generateStations(this.state.routes[lines.GREEN])}
        {generateStations(this.state.routes[lines.BLUE])}
        {generateStations(this.state.routes[lines.RED])}
        <LightRailLegend/>

      </Map>
    );
  }
}
