const generateStation = (name, location, openingDate) => {
  return {
    name,
    location,
    openingDate,
  }
};

const lines = {
  RED: "red",
  BLUE: "blue",
  GREEN: "green",
};

const statuses = {
  APPROVED: "Voter approved",
  PLANNING: "Planning",
  DESIGN: "Design",
  CONSTRUCTION: "Construction",
  COMPLETED: "Completed",
};

let federalWay = generateStation("Federal Way Transit Center", [47.3175, -122.304722], "2024-01-01");
let westlake = generateStation("Westlake", [47.611389, -122.337222], "2009-07-18");
let sodo = generateStation("SODO", [47.581278, -122.327389], "2009-07-18");
let angleLake = generateStation("Angle Lake", [47.422778, -122.2975], "2016-09-24");
let southFederalWay = generateStation("South Federal Way", [47.289598, -122.306539], "2030-01-01");
let kent = generateStation("Kent/Des Moines", [47.3892, -122.293], "2024-01-01");
let lynnwood = generateStation("Lynnwood Transit Center", [47.816111, -122.296389], "2024-01-01");
let shorelineSouth = generateStation("Shoreline South/145th", [47.733671, -122.324542], "2024-01-01");
let northGate = generateStation("Northgate Transit Center", [47.703056, -122.328056], "2021-01-01");
let overlakeTC = generateStation("Overlake Transit Center", [47.644444, -122.133333], "2023-01-01");
let universityOfWashington = generateStation("University of Washington", [47.649722, -122.303889], "2016-03-19");
let internationalDistrict = generateStation("International District/Chinatown", [47.598333, -122.328056], "2009-07-18");
let southEastRedmond = generateStation("SE Redmond", [47.6674, -122.1092], "2024-01-01");
let seatac = generateStation("SeaTac/Airport", [47.445169, -122.296875], "2009-12-19");
let capitolHill = generateStation("Capitol Hill", [47.6192, -122.3202], "2016-03-19");
let uDistrict = generateStation("U District", [47.660556, -122.314167], "2021-01-01");
let universityStreet = generateStation("University Street", [47.607778, -122.336], "2009-07-18");
let pioneerSquare = generateStation("Pioneer Square", [47.602778, -122.331389], "2009-07-18");
let stadium = generateStation("Stadium", [47.591333, -122.327167], "2009-07-18");
let beaconHill = generateStation("Beacon Hill", [47.579272, -122.311760], "2009-07-18");
let mtBaker = generateStation("Mount Baker", [47.576482, -122.297664], "2009-07-18");
let columbiaCity = generateStation("Columbia City", [47.559722, -122.292589], "2009-07-18");
let othello = generateStation("Othello", [47.537917, -122.281528], "2009-07-18");
let rainierBeach = generateStation("Rainier Beach", [47.522611, -122.279361], "2009-07-18");
let tukwila = generateStation("Tukwila International Boulevard", [47.464083, -122.288056], "2009-07-18");

const initiatives = {
  CENTRAL: {
    name: "Central Link",
    yearCompleted: "2009",
    stations: [
      westlake,
      universityStreet,
      pioneerSquare,
      internationalDistrict,
      stadium,
      sodo,
      beaconHill,
      mtBaker,
      columbiaCity,
      othello,
      rainierBeach,
      tukwila,
      seatac,
    ],
    connectsTo: null,
    status: statuses.COMPLETED
  },
  UNIVERSITY: {
    name: "University Link",
    yearCompleted: "2016",
    stations: [
      universityOfWashington,
      capitolHill,
    ],
    connectsTo: westlake,
    status: statuses.COMPLETED
  },
  ANGLE_LAKE: {
    name: "South 200th Link Extension",
    yearCompleted: "2016",
    stations: [
      angleLake
    ],
    connectsTo: seatac,
    status: statuses.COMPLETED
  },
  EAST_LINK: {
    name: "East Link Extension",
    yearCompleted: "2023",
    stations: [
      generateStation("Judkins Park", [47.590278, -122.301944], "2023-01-01"),
      generateStation("Mercer Island", [47.588056, -122.233333], "2023-01-01"),
      generateStation("South Bellevue", [47.586111, -122.191111], "2023-01-01"),
      generateStation("East Main", [47.61, -122.190833], "2023-01-01"),
      generateStation("Bellevue Transit Center", [47.615556, -122.193611], "2023-01-01"),
      generateStation("Wilburton", [47.618056, -122.183889], "2023-01-01"),
      generateStation("Spring District/120th ", [47.623627, -122.179448], "2023-01-01"),
      generateStation("Bel-Red/130th", [47.624994, -122.162936], "2023-01-01"),
      generateStation("Overlake Village", [47.63562, -122.138073], "2023-01-01"),
      overlakeTC,
    ],
    connectsTo: internationalDistrict,
    status: statuses.CONSTRUCTION
  },
  NORTHGATE: {
    name: "Northgate Extension",
    yearCompleted: "2021",
    stations: [
      northGate,
      generateStation("Roosevelt", [47.676667, -122.315556], "2021-01-01"),
      uDistrict,
    ],
    connectsTo: universityOfWashington,
    status: statuses.CONSTRUCTION
  },
  REDMOND: {
    name: "Redmond Extension",
    yearCompleted: "2024",
    stations: [
      southEastRedmond,
      generateStation("Downtown Redmond", [47.67224, -122.120156], "2024-01-01"),
    ],
    connectsTo: overlakeTC,
    status: statuses.PLANNING
  },
  LYNNWOOD: {
    name: "Lynwood Link Extension",
    yearCompleted: "2024",
    stations: [
      lynnwood,
      generateStation("Mountlake Terrace Transit Center", [47.785225, -122.316283], "2024-01-01"),
      generateStation("Shoreline North/185th", [47.763333, -122.323333], "2024-01-01"),
      shorelineSouth,
    ],
    connectsTo: northGate,
    status: statuses.DESIGN
  },
  FEDERAL: {
    name: "Federal Way Link Extension",
    yearCompleted: "2024",
    stations: [
      kent,
      generateStation("South 272nd Street", [47.3588, -122.2975], "2024-01-01"),
      federalWay,
    ],
    connectsTo: angleLake,
    status: statuses.COMPLETED
  },
  WEST: {
    name: "West Seattle Link Extension",
    yearCompleted: "2030",
    stations: [
      generateStation("Delridge", [47.569586, -122.362581], "2030-01-01"),
      generateStation("Avalon", [47.564069, -122.374375], "2030-01-01"),
      generateStation("Alaska Junction", [47.561084, -122.384135], "2030-01-01"),
    ],
    connectsTo: sodo,
    status: statuses.PLANNING
  },
  TACOMA: {
    name: "Tacoma Dome Extension",
    yearCompleted: "2030",
    stations: [
      southFederalWay,
      generateStation("Fife", [47.240939, -122.354980], "2030-01-01"),
      generateStation("East Tacoma", [47.239729, -122.404236], "2030-01-01"),
      generateStation("Tacoma Dome", [47.236939, -122.425828], "2030-01-01"),
    ],
    connectsTo: federalWay,
    status: statuses.PLANNING
  },
  BALLARD: {
    name: "Ballard Link Extension",
    yearCompleted: "2035",
    stations: [
      generateStation("Ballard", [47.668141, -122.376224], "2035-01-01"),
      generateStation("Interbay", [47.648066, -122.376277], "2035-01-01"),
      generateStation("Smith Cove", [47.628937, -122.370199], "2035-01-01"),
      generateStation("Seattle Center", [47.623360, -122.355585], "2035-01-01"),
      generateStation("South Lake Union", [47.622603, -122.344491], "2035-01-01"),
      generateStation("Denny", [47.619260, -122.338497], "2035-01-01"),
      westlake,
      generateStation("Midtown", [47.606916, -122.332112], "2035-01-01"),
      internationalDistrict,
      // skips Stadium
      sodo
    ],
    status: statuses.PLANNING
  },
  SOUTH_GRAHAM: {
    name: "S. Graham St. station",
    yearCompleted: "2031",
    stations: [
      generateStation("South Graham St", [47.545913, -122.285511], "2031-01-01")
    ],
    status: statuses.APPROVED
  },
  BOEING_ACCESS: {
    name: "S. Boeing Access Rd. station",
    yearCompleted: "2031",
    stations: [
      generateStation("South Boeing Access Road", [47.507813, -122.281651], "2031-01-01")
    ],
    status: statuses.APPROVED
  },
};

const routes = {
  [lines.RED]: [
    {initiative: initiatives.LYNNWOOD},
    {initiative: initiatives.NORTHGATE},
    {initiative: initiatives.UNIVERSITY},
    {
      initiative: {
        ...initiatives.CENTRAL,
        stations: [
          westlake,
          universityStreet,
          pioneerSquare,
          internationalDistrict,
          stadium,
          sodo,
        ]
      },
    },
    {initiative: initiatives.WEST},
  ],
  [lines.BLUE]: [
    {
      initiative: {
        ...initiatives.CENTRAL,
        stations: [
          westlake,
          universityStreet,
          pioneerSquare,
          internationalDistrict
        ]
      },
    },
    {initiative: initiatives.EAST_LINK},
    {initiative: initiatives.REDMOND},
  ],
  [lines.GREEN]: [
    {initiative: initiatives.BALLARD},
    {
      initiative: {
        ...initiatives.CENTRAL,
        stations: [
          beaconHill,
          mtBaker,
          columbiaCity,
          othello,
          rainierBeach,
          tukwila,
          seatac
        ],
        connectsTo: sodo
      },
    },
    {initiative: initiatives.SOUTH_GRAHAM},
    {initiative: initiatives.ANGLE_LAKE},
    {initiative: initiatives.BOEING_ACCESS},
    {initiative: initiatives.FEDERAL},
    {initiative: initiatives.TACOMA}
  ]
};

export {
  routes,
  lines,
  initiatives
}