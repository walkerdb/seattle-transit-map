const square = num => Math.pow(num, 2);

const getDistance = (point1, point2) => Math.sqrt(
  square(point2[0] - point1[0]) + square(point2[1] - point1[1])
);

const sortNextNearestDistance = (positions, sortedPositions) => {
  if (positions.length === 0) {
    return [positions, sortedPositions]
  }
  const currentPosition = positions.shift();
  sortedPositions.push(currentPosition);

  const sortedByDistanceFromCurrentStation = positions
    .map(position => [getDistance(currentPosition, position), position])
    .sort((a, b) => a[0] - b[0]);

  return [sortedByDistanceFromCurrentStation.map(position => position[1]), sortedPositions]
};

const sortNearestDistance = (positions, sortedPositions = []) => {
  while (positions.length !== 0) {
    [positions, sortedPositions] = sortNextNearestDistance(positions, sortedPositions);
  }
  return sortedPositions;
};

export {
  sortNearestDistance
}