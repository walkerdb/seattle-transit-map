import soundTransitRoutes from "../data/transit_routes/sound_transit";
import {transitColors} from "../Colors";
import {GeoJSON} from "react-leaflet";
import React from "react";

const ExistingLightRailRoutes = props => {
  const transitRoutesPopup = (feature, layer) => {
    const popupContent = `<div>${feature.properties.name}</div>`;
    layer.bindTooltip(popupContent, {sticky: true});
  }

  return <GeoJSON
          data={soundTransitRoutes}
          filter={feature => feature.properties.name === "Link light rail"}
          style={feature => {return {color: transitColors["lightRail"]}}}
          onEachFeature={transitRoutesPopup}
        />
};

export default ExistingLightRailRoutes