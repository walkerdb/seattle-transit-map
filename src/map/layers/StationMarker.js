import {CircleMarker, Tooltip} from "react-leaflet";
import {transitColors} from "../Colors";
import React from "react";

const StationMarker = (props) => (
  <div>
    <CircleMarker radius={5}
                  center={props.station.location}
                  stroke={false}
                  fillOpacity={Number(props.segment.initiative.yearCompleted) > 2018 ? 0.4 : 1}
                  opacity={Number(props.segment.initiative.yearCompleted) > 2018 ? 0.4 : 1}
                  color={transitColors["lightRail"]}
                  key={props.station.name}
    >
      <Tooltip>
        {props.station.name}
        <br/>
        ETA: {props.segment.initiative.yearCompleted}
        <br/>
        Project: {props.segment.initiative.name}
        <br/>
        Status: {props.segment.initiative.status}
      </Tooltip>
    </CircleMarker>
  </div>
);

export default StationMarker