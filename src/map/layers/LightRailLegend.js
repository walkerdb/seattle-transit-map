import {MapControl, withLeaflet} from 'react-leaflet'
import ReactDOMServer from 'react-dom/server';
import {initiatives} from "../LightRailInitiatives";
import {transitColors} from "../Colors"
import {Control, DomUtil} from "leaflet";
import React from "react"


export default withLeaflet(class LightRailLegend extends MapControl {
  createLeafletElement(_props) {
    var legend = new Control({position: 'bottomleft'});
    legend.onAdd = function (map) {
      let legendEntries = Object.values(initiatives)
        .sort((a, b) => Number(a.yearCompleted) - Number(b.yearCompleted))
        .map(initiative => (
            <div key={initiative.name}>
              <div className="colored-square" style={{background: transitColors["lightRail"], opacity: Number(initiative.yearCompleted) > 2018 ? 0.4 : 1}}/>
              {` ${initiative.yearCompleted} - ${initiative.name}`}
              <br/>
            </div>
          )
        );

      let innerDiv = (
        <div style={{margin: 7}}>
          <div style={{marginBottom: 4}}><strong>Upcoming Link Light Rail extensions</strong></div>
          {legendEntries}
        </div>
      );

      var div = DomUtil.create('div', 'info legend');
      div.innerHTML = ReactDOMServer.renderToStaticMarkup(innerDiv);
      return div
    };
    return legend;
  }
});
