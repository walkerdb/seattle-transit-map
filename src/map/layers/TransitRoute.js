import {sortNearestDistance} from "../Utilities";
import {transitColors} from "../Colors"
import {Polyline, Tooltip} from "react-leaflet";
import React from "react";

const TransitRoute = (props) => {
  const initiative = props.segment.initiative;
  let positions = initiative.stations.map(station => station.location);
  initiative.connectsTo && positions.unshift(initiative.connectsTo.location);

  if (initiative.name !== "Redmond Extension") {
    positions = sortNearestDistance(positions);
  }

  if (Number(initiative.yearCompleted) < 2018) {
    return <div></div>;
  }

  return (
    <Polyline
      color={transitColors["lightRail"]}
      opacity={Number(initiative.yearCompleted) > 2018 ? 0.4 : 1}
      weight={4}
      positions={positions}
      key={initiative.name}
    >
      <Tooltip sticky>
        {initiative.name}
        <br/>
        ETA: {initiative.yearCompleted}
        <br/>
        Status: {initiative.status}
      </Tooltip>
    </Polyline>
  );
}

export default TransitRoute