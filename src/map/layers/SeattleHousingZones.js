import housingZones from "../data/seattle_housing_zones";
import {GeoJSON} from "react-leaflet";
import React from "react";
import {zoneColors} from "../Colors";

const HousingZones = props => {
  const getColorForZone = (feature) => {
    switch (feature.properties.CLASS_DESC) {
      case "Multi-Family": {
        switch (feature.properties.ZONELUT_DE) {
          case "Lowrise 1":
          case "Lowrise 2":
          case "Lowrise 3":
          case "Lowrise 2 Incentive":
          case "Lowrise 3 Incentive":
            return zoneColors["Lowrise"];
          case "Midrise 1":
          case "Midrise Incentive":
            return zoneColors["Midrise"];
          case "Highrise": {
            return zoneColors["Highrise"];
          }
          default:
            return ""
        }
      }
      case "Neighborhood/Commercial": {
        switch (feature.properties.ZONELUT_DE) {
          case "Commercial 1":
          case "Commercial 1 Incentive":
          case "Commercial 2":
            return zoneColors["Auto Commerical"];
          default:
            return zoneColors["Neighborhood/Commercial"];
        }
      }
      default:
        return zoneColors[feature.properties.CLASS_DESC];
    }
  };

  const housingZonesPopup = (feature, layer) => {
    const popupContent = `<div>${feature.properties.CLASS_DESC}<br/>${feature.properties.ZONELUT_DE}</div>`;
    layer.bindTooltip(popupContent);
    layer.on('mouseover', () => layer.setStyle({...layer.options, "stroke": true}));
    layer.on('mouseout', () => layer.setStyle({...layer.options, "stroke": false}))
  };

  return (
    <GeoJSON
      data={housingZones}
      filter={feature => !!feature.properties.CLASS_DESC}
      style={feature => {
        const baseConfig = {stroke: false, fillOpacity: 0.5};
        return {...baseConfig, color: getColorForZone(feature)}
      }}
      onEachFeature={housingZonesPopup}
    />
  )
};

export default HousingZones